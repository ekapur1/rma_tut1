package com.example.tutorijal1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private fun showMessage() {
            // Pronaći ćemo naš edit text i text view na osnovu id-a
        val editText = findViewById<EditText>(R.id.editText1)
        val textView = findViewById<TextView>(R.id.textView1)
            // Tekst ćemo prebaciti u varijablu
        val message = editText.text.toString()
            // Postavimo tekst
        textView.text = message
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button = findViewById<Button>(R.id.button1);
        button.setOnClickListener {
            showMessage()
        }
    }
}